﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public Transform[] Spawnpoints;
    public GameObject[] objectsToSpawn;

  
    public float timeBetSpawn;
    float currentTime = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       {
            if (currentTime <= 0)
            {
                Spawn();
                currentTime = timeBetSpawn;

            }
            currentTime -= Time.deltaTime;
        }
    }

    void Spawn()
    {
        Instantiate(objectsToSpawn[Random.Range(0,objectsToSpawn.Length)],Spawnpoints[Random.Range(0,Spawnpoints.Length)].position,Quaternion.identity);
        //Debug.Log("Spawned");
       
    }
}
