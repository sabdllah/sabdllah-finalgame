﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class player : MonoBehaviour
{
    private Rigidbody2D myBody;

    public float speed;

    public Text pointsText;


    // Start is called before the first frame update
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
    }

     void Update()
    {
        pointsText.text = points.ToString();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        if (h > 0)

            myBody.velocity = Vector2.right * speed;
        else if (h < 0)

            myBody.velocity = Vector2.left * speed;
        else
            myBody.velocity = Vector2.zero;


    }

    public int points = 0;
    private void OnCollisionEnter2D(Collision2D collision)
{
        if (collision.gameObject.layer == 9)
        {
            if(points>0)
            points--;
        Destroy(collision.gameObject);
        }
        else if (collision.gameObject.layer == 8)
        {
            points++;
            Destroy(collision.gameObject);
        }
}






}
